import { Box, Text } from "@chakra-ui/react";
import React from "react";

const Error = () => {
  return (
    <Box
      bg={"white"}
      color={"red"}
      width={"50vw"}
      display={"flex"}
      justifyContent={"center"}
      alignItems={"center"}
      m={"auto"}
      marginTop={"5rem"}
      p={"10rem"}
      borderRadius={"2rem"}
    >
      <Text fontSize={"1.2rem"}>Error Occurred Please Try After Some Time</Text>
    </Box>
  );
};

export default Error;
