import { Box, Text } from "@chakra-ui/react";
import React from "react";
import { Link } from "react-router-dom";
import Lists from "./Lists";
import { Card, CardHeader, CardBody, CardFooter } from "@chakra-ui/react";

import randomColor from "randomcolor";

const BoardItem = ({ id, name, bgclr }) => {
  return (
    <Link to={`/boards/${id}`} element={<Lists />}>
      <Card
        bg={bgclr}
        height={"10rem"}
        width={"18rem"}
        borderRadius={"0.2rem"}
        fontSize={"1.5rem"}
        color={"white"}
        cursor={"pointer"}
        py={4}
        px={4}
        fontWeight={600}
      >
        <CardBody>
          <Text>{name}</Text>
        </CardBody>
      </Card>
    </Link>
  );
};

export default BoardItem;
