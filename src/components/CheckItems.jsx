import React, { useEffect, useState } from "react";
import axios from "axios";
import {
  Box,
  Button,
  Checkbox,
  FormControl,
  Input,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Text,
} from "@chakra-ui/react";
import { useToast } from "@chakra-ui/react";
import { IoIosMore } from "react-icons/io";
import { Progress } from "@chakra-ui/react";
import { Tooltip } from "@chakra-ui/react";
import {
  deleteItem,
  getData,
  postData,
  postItem,
  putCheckItems,
} from "../api/api";
import AddCard from "./AddCard";

const CheckItems = ({ checklistId, idCard }) => {
  const [checkItems, setCheckItems] = useState([]);
  const [itemSelected, setitemSelected] = useState(false);
  const [newItem, setNewItem] = useState("");
  const [taskCompleted, setTaskCompleted] = useState(0);
  const [error, setError] = useState(false);

  const toast = useToast();
  const Key = import.meta.env.VITE_KEY;
  const Token = import.meta.env.VITE_TOKEN;

  useEffect(() => {
    getData("checklists", "checkItems", setCheckItems, checklistId)
      .then((res) => {
        const initialCompletedCount = res.filter(
          (item) => item.state === "complete"
        ).length;
        setTaskCompleted(initialCompletedCount);
      })
      .catch((err) => {
        toast({
          title: " Error Occurred ",
          description: `Error: ${err}.`,
          status: "error",
          duration: 9000,
          isClosable: true,
        });
        setError(true);
      });
  }, [checklistId]);

  const handelItemAdd = () => {
    if (newItem == "") {
      toast({
        title: "Please Add a Item",
        description: `Please Provide a value to Your Item`,
        status: "warning",
        duration: 9000,
        isClosable: true,
      });
      return;
    }
    postItem(checklistId, newItem)
      .then((res) => {
        setCheckItems((oldItems) => [...oldItems, res.data]);

        toast({
          title: "Item Added ",
          description: `Item Added to Your CheckList with name ${newItem}`,
          status: "success",
          duration: 9000,
          isClosable: true,
        });
        setNewItem("");

        if (res.data.state === "complete") {
          setTaskCompleted((prevCount) => prevCount + 1);
        }
      })
      .catch((err) => {
        toast({
          title: " Error Occurred ",
          description: `Error: ${err}.`,
          status: "error",
          duration: 9000,
          isClosable: true,
        });
        setError(true);
      });
  };

  const handelDeleteItem = (checkItemid) => {
    deleteItem(checklistId, checkItemid)
      .then((res) => {
        setCheckItems((oldcheckItems) => {
          return oldcheckItems.filter(
            (checkItem) => checkItem.id != checkItemid
          );
        });

        toast({
          title: "List Item Deleted Successfully.",
          description: `We've Deleted your this Item from checkList.`,
          status: "success",
          duration: 9000,
          isClosable: true,
        });

        const deletedItem = checkItems.find((item) => item.id === checkItemid);
        if (deletedItem.state === "complete") {
          setTaskCompleted((prevCount) => prevCount - 1);
        }
      })
      .catch((err) => {
        toast({
          title: " Error Occurred ",
          description: `Error: ${err}.`,
          status: "error",
          duration: 9000,
          isClosable: true,
        });
        setError(true);
      });
  };

  const handelUpdateItems = (id, state) => {
    console.log("Inside Update Fn");
    let newstate = state == "complete" ? "incomplete" : "complete";
    console.log(newstate);

    putCheckItems(idCard, id, newstate)
      .then((res) => {
        console.log("from updated", res.data);

        setCheckItems((oldItems) => {
          let cnt = 0;
          const newItems = oldItems.map((item) => {
            if (item.id == res.data.id) {
              item.state = newstate;
            }

            //checkeing for completed state

            if (item.state == "complete") {
              cnt = cnt + 1;
            }

            return item;
          });

          setTaskCompleted(cnt);
          return newItems;
        });
      })
      .catch((err) => {
        toast({
          title: " Error Occurred ",
          description: `Error: ${err}.`,
          status: "error",
          duration: 9000,
          isClosable: true,
        });
        setError(true);
      });
  };

  console.log("checkites", checkItems);

  if (error) {
    return (
      <>
        <Box textAlign={"center"} color={"red"} fontSize={"1.5rem"}>
          Error Occured...
        </Box>
      </>
    );
  }

  return (
    <>
      <Box
        display={"flex"}
        flexDirection={"column"}
        gap={"1.5rem"}
        paddingBottom={"1rem"}
      >
        {checkItems.length > 0 ? (
          <Tooltip
            hasArrow
            label={((taskCompleted / checkItems.length) * 100).toFixed(2)}
            bg="blue.500"
            color={"white"}
          >
            <Progress
              cursor={"pointer"}
              hasStripe
              value={(taskCompleted / checkItems.length) * 100}
              isAnimated
              style={{ transition: "width 0.3s ease-in-out" }}
            />
          </Tooltip>
        ) : (
          ""
        )}
        {checkItems?.map((checkItem) => {
          return (
            <Box
              key={checkItem.id}
              display={"flex"}
              justifyContent={"space-between"}
              fontSize={"1.2rem"}
            >
              <Box display={"flex"} gap={"4rem"}>
                <Checkbox
                  colorScheme="green"
                  onChange={(e) => {
                    handelUpdateItems(checkItem.id, checkItem.state);
                  }}
                  isChecked={checkItem.state == "complete" ? true : false}
                />
                <Text>{checkItem.name}</Text>
              </Box>
              <Box>
                <Menu>
                  <MenuButton>
                    <IoIosMore fontSize={"1.8rem"} />
                  </MenuButton>
                  <MenuList bg={"#1DA1F2"}>
                    <MenuItem
                      bg={"#1DA1F2"}
                      onClick={(e) => {
                        handelDeleteItem(checkItem.id);
                      }}
                    >
                      Archive this Item/Delete
                    </MenuItem>
                  </MenuList>
                </Menu>
              </Box>
            </Box>
          );
        })}
        {!itemSelected ? (
          <Button
            display={"flex"}
            justifyContent={"flex-start"}
            width={"10rem"}
            fontSize={"1.2rem"}
            onClick={() => {
              setitemSelected(true);
            }}
          >
            <Text>Add an Item</Text>
          </Button>
        ) : (
          <Box>
            <AddCard
              method={handelItemAdd}
              setData={setitemSelected}
              placeholder={"Add an item"}
              setName={setNewItem}
              buttonTitle={"Add Item"}
              name={newItem}
            />

          </Box>
        )}
      </Box>
    </>
  );
};

export default CheckItems;
