import React from "react";
import { ImHome } from "react-icons/im";
import { Link } from "react-router-dom";
import { Box } from "@chakra-ui/react";
import logo from "../assets/logo.svg";
const Header = () => {
  return (
    <Box
      bg={"lightskyblue"}
      py={4}
      px={10}
      display={"flex"}
      justifyContent={"center"}
      alignItems={"center"}
      gap={10}
    >
      <Link to="/">
        <img width={"150rem"} src={logo} />
      </Link>
    </Box>
  );
};

export default Header;
