import React from "react";
import {
  Box,
  Button,
  Center,
  Flex,
  FormControl,
  FormLabel,
  Input,
  Spinner,
  Text,
  useToast,
} from "@chakra-ui/react";
import CheckList from "./CheckList";
import axios from "axios";
import { IoMdAdd } from "react-icons/io";
import { useEffect, useState } from "react";
import {
  Popover,
  PopoverTrigger,
  PopoverContent,
  PopoverHeader,
  PopoverBody,
  PopoverArrow,
  PopoverCloseButton,
} from "@chakra-ui/react";
import Error from "./Error";
import { getData, postData } from "../api/api";

const CardModalData = ({ card }) => {
  const [checklists, setChecklists] = useState([]);
  const [checkListName, setCheckListName] = useState("");
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);
  const toast = useToast();

  const Key = import.meta.env.VITE_KEY;
  const Token = import.meta.env.VITE_TOKEN;

  useEffect(() => {
    getData("cards", "checklists", setChecklists, card.id)
      .then((res) => {
        setLoading(false);
      })
      .catch((err) => {
        setLoading(false);
        setError(true);

        toast({
          title: " Error Occurred ",
          description: `Error: ${err}.`,
          status: "error",
          duration: 9000,
          isClosable: true,
        });
      });
  }, []);

  const handelAddCheckList = () => {
    if (checkListName === "") {
      toast({
        title: "Please Add a CheckList",
        description: `Please Provide a value to Your CheckList`,
        status: "warning",
        duration: 9000,
        isClosable: true,
      });
      return;
    }

    const Key = import.meta.env.VITE_KEY;
    const Token = import.meta.env.VITE_TOKEN;


    postData("checklists", checkListName, setChecklists, "idCard", card.id)
      .then((res) => {
        toast({
          title: "Added a CheckList",
          description: `We have added a CheckList with Name: ${checkListName}`,
          status: "success",
          duration: 9000,
          isClosable: true,
        });
        setCheckListName("");
      })
      .catch((err) => {
        setError(true);
        toast({
          title: " Error Occurred ",
          description: `Error: ${err}.`,
          status: "error",
          duration: 9000,
          isClosable: true,
        });
      });
  };

  console.log("check", checklists);

  if (error) {
    return (
      <>
        <Box textAlign={"center"} color={"red"} fontSize={"1.5rem"}>
          Error Occured...
        </Box>
      </>
    );
  }

  return (
    <>
      {loading ? (
        <Center>
          <Spinner
            margin={"2rem"}
            thickness="8px"
            speed="0.65s"
            emptyColor="gray.200"
            color="blue.500"
            size="xl"
          />
        </Center>
      ) : (
        <Flex flexDir={"column"}>
          <Box display={"flex"} justifyContent={"space-between"}>
            <Text>CHECKLIST</Text>

            <Popover>
              <PopoverTrigger>
                <Button
                  display={"flex"}
                  justifyContent={"flex-start"}
                  gap={"1rem"}
                  bg={"transparent"}
                  color={"#9DABBA"}
                  fontSize={"1.2rem"}
                >
                  <IoMdAdd fontSize={"1.5rem"} />
                  <Text>Add CheckList</Text>
                </Button>
              </PopoverTrigger>
              <PopoverContent>
                <PopoverArrow />
                <PopoverCloseButton />
                <PopoverHeader textAlign={"center"}>
                  Add a Checklist Name
                </PopoverHeader>
                <PopoverBody>
                  <FormControl>
                    <FormLabel fontWeight={600}>Title</FormLabel>
                    <Input
                      boxShadow={"0px 5px 10px -10px black"}
                      bg={"#22272B"}
                      outline={"none"}
                      border={"none"}
                      p={"1rem 1rem"}
                      borderRadius={"0.4rem"}
                      type="text"
                      placeholder="Enter a title for Checklist... "
                      fontWeight={600}
                      color={"#9DABBA"}
                      value={checkListName}
                      onChange={(e) => setCheckListName(e.target.value)}
                    />
                    <Button
                      colorScheme="blue"
                      color={"#22272B"}
                      m={"1rem 0rem"}
                      padding={"1rem 1.5rem"}
                      onClick={(e) => {
                        handelAddCheckList();
                      }}
                    >
                      <Text>Add</Text>
                    </Button>
                  </FormControl>
                </PopoverBody>
              </PopoverContent>
            </Popover>
          </Box>

          <hr />
          <Box display={"flex"} flexDirection={"column"} margin={"1rem  2rem"}>
            {checklists?.map((checklist) => {
              return (
                <CheckList
                  key={checklist.id}
                  checklist={checklist}
                  checklists={checklists}
                  setChecklists={setChecklists}
                ></CheckList>
              );
            })}
          </Box>
        </Flex>
      )}
    </>
  );
};

export default CardModalData;
