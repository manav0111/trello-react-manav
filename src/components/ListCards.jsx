import React, { useState, useEffect } from "react";
import {
  Button,
  Box,
  Flex,
  Text,
  FormControl,
  Input,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  useDisclosure,
  Spinner,
  Center,
} from "@chakra-ui/react";
import axios from "axios";
import { IoCloseSharp } from "react-icons/io5";
import { useToast } from "@chakra-ui/react";
import { IoMdAdd } from "react-icons/io";
import CardModalData from "./CardModalData";
import { deleteData, getData, postData } from "../api/api";
import AddCard from "./AddCard";

function ListCards({ listId }) {
  const [cards, setCards] = useState([]);
  const [createdCard, setCreatedCard] = useState(false);
  const [newCardName, setNewCardName] = useState("");
  const [selectedCard, setSelectedCard] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);
  const toast = useToast();

  const {
    isOpen: isModalOpen,
    onOpen: openModal,
    onClose: closeModal,
  } = useDisclosure();

  const Key = import.meta.env.VITE_KEY;
  const Token = import.meta.env.VITE_TOKEN;

  useEffect(() => {
    getData("lists", "cards", setCards, listId)
      .then((response) => {
        setLoading(false);
      })
      .catch((err) => {
        setLoading(false);
        toast({
          title: " Error Occurred Not able to display Cards ",
          description: `Error: ${err}.`,
          status: "error",
          duration: 9000,
          isClosable: true,
        });
        setError(true);
      });
  }, []);

  const handleOpenModal = (card) => {
    setSelectedCard(card);
    openModal();
  };

  const handleCloseModal = () => {
    setSelectedCard(null);
    closeModal();
  };

  const handleAddCard = () => {
    if (newCardName === "") {
      toast({
        title: "Please Add a Card",
        description: `Please Provide a value to Your Card`,
        status: "warning",
        duration: 9000,
        isClosable: true,
      });
      return;
    }

    postData("cards", newCardName, setCards, "idList", listId)
      .then((res) => {
        toast({
          title: "New Card Added.",
          description: `We've created a new Card with Name: ${newCardName}.`,
          status: "success",
          duration: 9000,
          isClosable: true,
        });
        setNewCardName("");
      })
      .catch((err) => {
        toast({
          title: " Error Occurred ",
          description: `Error: ${err}.`,
          status: "error",
          duration: 9000,
          isClosable: true,
        });
        setError(true);
      });
  };

  const handleDeleteCard = (id) => {
    deleteData("cards", id)
      .then(() => {
        setCards((oldCards) => {
          return oldCards.filter((card) => card.id !== id);
        });
        toast({
          title: "Card Deleted Successfully.",
          description: `We've Deleted your Card.`,
          status: "success",
          duration: 9000,
          isClosable: true,
        });
      })
      .catch((err) => {
        toast({
          title: " Error Occurred ",
          description: `Error: ${err}.`,
          status: "error",
          duration: 9000,
          isClosable: true,
        });
        setError(true);
      });
  };

  if (error) {
    return (
      <>
        <Box color={"red"}>Error Occured...</Box>
      </>
    );
  }

  return (
    <Box>
      {loading ? (
        <Center>
          <Spinner
            margin={"2rem"}
            thickness="8px"
            speed="0.65s"
            emptyColor="gray.200"
            color="blue.500"
            size="xl"
          />
        </Center>
      ) : (
        <Flex direction={"column"} gap={"1.5rem"}>
          {cards.map((card, index) => (
            <Box
              key={card.id}
              boxShadow={"0px 5px 10px -10px black"}
              bg={"#22272B"}
              p={"1rem 1rem"}
              borderRadius={"1.2rem"}
              display={"flex"}
              justifyContent={"space-between"}
              onClick={(e) => {
                e.stopPropagation();
                handleOpenModal(card);
              }}
            >
              <Text fontSize={"0.9rem"}>{card.name}</Text>
              <IoCloseSharp
                onClick={(e) => {
                  e.stopPropagation();
                  handleDeleteCard(card.id);
                }}
              />
            </Box>
          ))}
          {!createdCard ? (
            <Button
              display={"flex"}
              justifyContent={"flex-start"}
              gap={"1rem"}
              bg={"transparent"}
              color={"#9DABBA"}
              fontSize={"1.2rem"}
              _hover={{ bg: "#22272B" }}
              onClick={() => setCreatedCard(true)}
            >
              <IoMdAdd fontSize={"1.5rem"} />
              <Text>Add a Card</Text>
            </Button>
          ) : (
            <Box>
              <AddCard
                method={handleAddCard}
                setData={setCreatedCard}
                placeholder={"Enter a title for this card..."}
                setName={setNewCardName}
                buttonTitle={"Add card"}
                name={newCardName}
              />
             
            </Box>
          )}
        </Flex>
      )}

      <Modal isOpen={isModalOpen} onClose={handleCloseModal} size={"3xl"}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader textAlign={"center"} fontSize={"1.5rem"}>
            {selectedCard?.name}
          </ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            {selectedCard && (
              <CardModalData card={selectedCard} onClose={handleCloseModal} />
            )}
          </ModalBody>
          <ModalFooter>
            <Button colorScheme="blue" mr={3} onClick={handleCloseModal}>
              Close
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </Box>
  );
}

export default ListCards;
