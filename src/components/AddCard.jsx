import { Box, Button, FormControl, Input } from "@chakra-ui/react";
import React from "react";
import { IoCloseSharp } from "react-icons/io5";

const AddCard = ({
  method,
  setData,
  placeholder,
  setName,
  buttonTitle,
  name,
}) => {
  return (
    <>
      <form
        onSubmit={(e) => {
          e.preventDefault();
          setName("");
          //   setData(false);
          method();
        }}
      >
        <FormControl>
          <Input
            boxShadow={"0px 5px 10px -10px black"}
            bg={"#22272B"}
            outline={"none"}
            border={"none"}
            p={"2rem 1rem"}
            borderRadius={"0.7rem"}
            type="text"
            placeholder={placeholder}
            fontWeight={600}
            color={"#9DABBA"}
            onChange={(e) => setName(e.target.value)}
            value={name}
          />
          <Box
            display={"flex"}
            alignItems={"center"}
            gap={"1rem"}
            marginTop={"1rem"}
          >
            <Button colorScheme="blue" color={"#22272B"} type="submit">
              {buttonTitle}
            </Button>
            <IoCloseSharp
              style={{ cursor: "pointer" }}
              onClick={() => setData(false)}
              fontSize={"1.8rem"}
            />
          </Box>
        </FormControl>
      </form>
    </>
  );
};

export default AddCard;
