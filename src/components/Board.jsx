import React, { useEffect, useState } from "react";
import axios from "axios";
import { Box, Center, Spinner, Wrap, WrapItem } from "@chakra-ui/react";
import { getData, postData } from "../api/api.js";
import BoardItem from "./BoardItem.jsx";
import AddBoard from "./AddBoard.jsx";
import { useToast } from "@chakra-ui/react";
import Error from "./Error.jsx";

const Key = import.meta.env.VITE_KEY;
const Token = import.meta.env.VITE_TOKEN;

const Board = () => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);
  const toast = useToast();

  useEffect(() => {
    getData("members", "boards", setData)
      .then(() => {
        setLoading(false);
      })
      .catch((err) => {
        setLoading(false);
        setError(true);
        toast({
          title: " Error Occurred ",
          description: `Error: ${err}.`,
          status: "error",
          duration: 9000,
          isClosable: true,
        });
      });
  }, []);

  const makeNewBoard = (name) => {
    postData("boards", name, setData)
      .then((res) => {
        toast({
          title: "Board Created.",
          description: `We've created your Board with Name: ${name}.`,
          status: "success",
          duration: 9000,
          isClosable: true,
        });
      })
      .catch((err) => {
        setError(true);

        toast({
          title: " Error Occurred ",
          description: `Error: ${err}.`,
          status: "error",
          duration: 9000,
          isClosable: true,
        });
      });
  };

  if (error) {
    return (
      <>
        <Error />
      </>
    );
  }

  return (
    <>
      {loading ? (
        <Center>
          <Spinner
            margin={"2rem"}
            thickness="8px"
            speed="0.65s"
            emptyColor="gray.200"
            color="blue.500"
            size="xl"
          />
        </Center>
      ) : (
        <Box
          display={"flex"}
          justifyContent="center"
          alignItems="center"
          columnGap={"1rem"}
          margin={"2rem"}
        >
          <Wrap gap={"100rem"}>
            <AddBoard makeNewBoard={makeNewBoard} />
            {data?.map((board, index) => (
              <WrapItem key={board.id} gap={"8rem"}>
                <Box>
                  <BoardItem
                    id={board.id}
                    name={board.name}
                    bgclr={board.prefs.backgroundColor}
                  />
                </Box>
              </WrapItem>
            ))}
          </Wrap>
        </Box>
      )}
    </>
  );
};

export default Board;
