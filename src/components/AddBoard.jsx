import React, { useState } from "react";
import {
  Button,
  Input,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  useDisclosure,
  Box,
  Text,
  FormControl,
  FormLabel,
} from "@chakra-ui/react";

const AddBoard = ({ makeNewBoard }) => {
  const [search, setSearch] = useState("");
  const { isOpen, onOpen, onClose } = useDisclosure();
  return (
    <>
      <Button
        height={"10rem"}
        width={"18rem"}
        borderRadius={"0.5rem"}
        fontSize={"1.5rem"}
        cursor={"pointer"}
        onClick={onOpen}
      >
        Create A Board
      </Button>

      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Add a Board Name</ModalHeader>
          <ModalCloseButton />
          <form
            onSubmit={(e) => {
              e.preventDefault();
              makeNewBoard(search);
              onClose();
            }}
          >
            <FormControl>
              <ModalBody>
                <FormLabel htmlFor="card-name">Card Name</FormLabel>

                <Input
                  type="text"
                  placeholder="Enter the board name"
                  onChange={(e) => {
                    setSearch(e.target.value);
                  }}
                />
              </ModalBody>

              <ModalFooter>
                <Button colorScheme="blue" mr={3} onClick={onClose}>
                  Close
                </Button>
                <Button type="submit" variant="ghost">
                  Made A Board
                </Button>
              </ModalFooter>
            </FormControl>
          </form>
        </ModalContent>
      </Modal>
    </>
  );
};

export default AddBoard;
