import { Box, Button, Text } from "@chakra-ui/react";
import React, { useState } from "react";
import { FaCalendarCheck } from "react-icons/fa";
import axios from "axios";
import { useToast } from "@chakra-ui/react";
import CheckItems from "./CheckItems";
import { deleteData } from "../api/api";

const CheckList = ({ checklist, checklists, setChecklists }) => {
  const [error, setError] = useState(false);

  console.log("checklist", checklist);
  const toast = useToast();

  const handeldeleteCheckList = (checkid) => {
    console.log("inside delete fn");
    const Key = import.meta.env.VITE_KEY;
    const Token = import.meta.env.VITE_TOKEN;


    deleteData("checklists", checkid)
      .then(() => {
        const updated = checklists.filter(
          (checklist) => checklist.id !== checkid
        );
        setChecklists(updated);
        toast({
          title: "CheckList Deleted Successfully.",
          description: `We've deleted your checklist.`,
          status: "success",
          duration: 9000,
          isClosable: true,
        });
      })
      .catch((err) => {
        setError(true);
        toast({
          title: " Error Occurred ",
          description: `Error: ${err}.`,
          status: "error",
          duration: 9000,
          isClosable: true,
        });
      });
  };

  if (error) {
    return (
      <>
        <Box textAlign={"center"} color={"red"} fontSize={"1.5rem"}>
          Error Occured...
        </Box>
      </>
    );
  }

  return (
    <>
      <Box
        display={"flex"}
        justifyContent={"space-between"}
        alignItems={"center"}
      >
        <Box
          display={"flex"}
          gap={"4rem"}
          fontSize={"1.2rem"}
          paddingTop={"1rem"}
        >
          <FaCalendarCheck />
          <Text>{checklist.name}</Text>
        </Box>
        <Button
          colorScheme="red"
          m={"1rem 0rem"}
          padding={"1rem 1.5rem"}
          onClick={(e) => {
            e.stopPropagation();
            handeldeleteCheckList(checklist.id);
          }}
        >
          <Text>Delete</Text>
        </Button>
      </Box>
      <CheckItems checklistId={checklist.id} idCard={checklist.idCard} />
    </>
  );
};

export default CheckList;
