import axios from "axios";

const Key = import.meta.env.VITE_KEY;
const Token = import.meta.env.VITE_TOKEN;

const baseUrl = "https://api.trello.com/1";

// Axios get Request

export const getData = (from, item, setData, id = "me") => {
  return new Promise((resolve, rej) => {
    axios
      .get(`${baseUrl}/${from}/${id}/${item}?key=${Key}&token=${Token}`)
      .then((res) => {
        setData(res.data);
        resolve(res.data);
      })
      .catch((err) => {
        rej(err);
      });
  });
};

// Post Axios Request

export const postData = (
  from,
  nameitem,
  setData,
  extraid = "",
  idvalue = ""
) => {
  if (extraid == "") {
    return new Promise((resolve, rej) => {
      axios
        .post(`${baseUrl}/${from}/?name=${nameitem}&key=${Key}&token=${Token}`)
        .then((res) => {
          setData((oldData) => [...oldData, res.data]);
          resolve(res.data);
        })
        .catch((err) => {
          rej(err);
        });
    });
  } else {
    return new Promise((resolve, rej) => {
      axios
        .post(
          `${baseUrl}/${from}/?name=${nameitem}&${extraid}=${idvalue}&key=${Key}&token=${Token}`
        )
        .then((res) => {
          setData((oldData) => [...oldData, res.data]);
          resolve(res.data);
        })
        .catch((err) => {
          rej(err);
        });
    });
  }
};

export const postItem = (checklistId, newItem) => {
  return new Promise((resolve, reject) => {
    axios
      .post(
        `https://api.trello.com/1/checklists/${checklistId}/checkItems?name=${newItem}&key=${Key}&token=${Token}`
      )
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err);
      });
  });
};

// Put Axios Request

export const putCheckItems = (idCard, id, newstate) => {
  return new Promise((resolve, reject) => {
    axios
      .put(
        `https://api.trello.com/1/cards/${idCard}/checkItem/${id}?state=${newstate}&key=${Key}&token=${Token}`
      )
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err);
      });
  });
};

export const putArchiveList = (id) => {
  return new Promise((resolve, reject) => {
    axios
      .put(
        `https://api.trello.com/1/lists/${id}?closed=true&key=${Key}&token=${Token}`
      )
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err);
      });
  });
};

// Delete Axios Request-

export const deleteData = (from, id) => {
  return new Promise((resolve, reject) => {
    axios
      .delete(
        `https://api.trello.com/1/${from}/${id}?key=${Key}&token=${Token}`
      )
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err);
      });
  });
};

export const deleteItem = (checklistId, checkItemid) => {
  return new Promise((resolve, reject) => {
    axios
      .delete(
        `https://api.trello.com/1/checklists/${checklistId}/checkItems/${checkItemid}?key=${Key}&token=${Token}`
      )
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err);
      });
  });
};
