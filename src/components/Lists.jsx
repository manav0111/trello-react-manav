import React, { useEffect, useState } from "react";
import { useParams, useNavigate, Link } from "react-router-dom";
import {
  Box,
  Button,
  Card,
  CardHeader,
  Center,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Spinner,
  Text,
  useDisclosure,
} from "@chakra-ui/react";
import { List, ListItem, ListIcon, UnorderedList } from "@chakra-ui/react";
import randomColor from "randomcolor";
import { IoMdAdd } from "react-icons/io";
import { IoIosMore } from "react-icons/io";
import { IoCloseSharp } from "react-icons/io5";
import { getData, postData, putArchiveList } from "../api/api.js";
import { useToast } from "@chakra-ui/react";
import axios from "axios";
import ListCards from "./ListCards.jsx";
import Error from "./Error.jsx";
import AddCard from "./AddCard.jsx";
// const { isOpen, onOpen, onClose } = useDisclosure();

const Lists = () => {
  const [listdata, setListData] = useState([]);
  const [newListName, setNewListName] = useState("");
  const [createdlist, setCreatedList] = useState(false);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);
  const { id } = useParams();
  const toast = useToast();
  const Key = import.meta.env.VITE_KEY;
  const Token = import.meta.env.VITE_TOKEN;

  useEffect(() => {
    getData("boards", "lists", setListData, id)
      .then(() => {
        setLoading(false);
      })
      .catch((err) => {
        setLoading(false);
        setError(true);
        toast({
          title: " Error Occurred ",
          description: `Error: ${err}.`,
          status: "error",
          duration: 9000,
          isClosable: true,
        });
      });
  }, []);

  const handleAddList = () => {
    postData("lists", newListName, setListData, "idBoard", id)
      .then((res) => {
        toast({
          title: "New List Created.",
          description: `We've created your new List with Name: ${newListName}.`,
          status: "success",
          duration: 9000,
          isClosable: true,
        });
      })
      .catch((err) => {
        setError(true);
        toast({
          title: " Error Occurred ",
          description: `Error: ${err}.`,
          status: "error",
          duration: 9000,
          isClosable: true,
        });
      });
  };

  const archiveList = (id) => {
    console.log("Inside archive list");

    putArchiveList(id)
      .then((res) => {
        console.log(res);
        setListData(listdata.filter((list) => list.id !== res.data.id));
        toast({
          title: "List Deleted Successfully.",
          description: `We've Deleted your List.`,
          status: "success",
          duration: 9000,
          isClosable: true,
        });
      })
      .catch((err) => {
        setError(true);
        toast({
          title: " Error Occurred ",
          description: `Error: ${err}.`,
          status: "error",
          duration: 9000,
          isClosable: true,
        });
      });
  };

  console.log(listdata);

  console.log(newListName);

  if (error) {
    return (
      <>
        <Error />
      </>
    );
  }

  return (
    <>
      {loading ? (
        <Center>
          <Spinner
            margin={"2rem"}
            thickness="8px"
            speed="0.65s"
            emptyColor="gray.200"
            color="blue.500"
            size="xl"
          />
        </Center>
      ) : (
        <Box padding={"2rem"} overflowX="auto">
          <Link to={"/boards"}>
            <Button padding={"1.5rem"} colorScheme="blue">
              Go Back
            </Button>
          </Link>

          <Box
            display={"flex"}
            alignItems="flex-start"
            padding={"1rem 0rem"}
            gap={"4rem"}
            minWidth="100%"
          >
            {listdata?.map((list, index) => (
              <Card
                textDecoration={"none"}
                key={list.id}
                boxSize={"0px 5px 10px -7px black"}
                bg={"#101204"}
                // width={"20rem"}
                borderRadius={"0.8rem"}
                fontSize={"1.2rem"}
                cursor={"pointer"}
                color={"white"}
                py={1}
                px={4}
                pb={8}
                fontWeight={600}
                minWidth="20rem"
                flexShrink={0}
              >
                <List>
                  <ListItem>
                    <Box display="flex" justifyContent="space-between">
                      <CardHeader fontSize={"1.5rem"}>{list.name}</CardHeader>
                      <Menu>
                        <MenuButton>
                          <IoIosMore fontSize={"1.8rem"} />
                        </MenuButton>
                        <MenuList bg={"#1DA1F2"}>
                          <MenuItem
                            bg={"#1DA1F2"}
                            onClick={(e) => {
                              archiveList(list.id);
                            }}
                          >
                            Archive this list/Delete
                          </MenuItem>
                        </MenuList>
                      </Menu>
                    </Box>
                  </ListItem>
                  <ListItem>
                    <ListCards listId={list.id} />
                  </ListItem>
                </List>
              </Card>
            ))}
            {!createdlist ? (
              <Button
                colorScheme="twitter"
                height={"3rem"}
                minWidth="18rem"
                borderRadius={"0.5rem"}
                cursor={"pointer"}
                gap={"1rem"}
                fontSize={"1.2rem"}
                display={"flex"}
                alignItems={"center"}
                onClick={() => {
                  setCreatedList(true);
                }}
              >
                <IoMdAdd />
                <Text>Add Another List</Text>
              </Button>
            ) : (
              <Card
                textDecoration={"none"}
                boxSize={"0px 5px 10px -7px black"}
                bg={"#101204"}
                minwidth="18rem"
                borderRadius={"0.5rem"}
                fontSize={"1.2rem"}
                cursor={"pointer"}
                color={"white"}
                py={4}
                px={4}
                fontWeight={600}
                flexShrink={0}
              >
                <CardHeader padding={"0.8rem"}>Add a List</CardHeader>
                <AddCard
                  method={handleAddList}
                  setData={setCreatedList}
                  placeholder={"Enter the list name"}
                  setName={setNewListName}
                  buttonTitle={" Create List"}
                  name={newListName}
                />
              </Card>
            )}
          </Box>
        </Box>
      )}
    </>
  );
};

export default Lists;
